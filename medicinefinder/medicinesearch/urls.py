from django.urls import path, include
from .views import *
from django.conf import settings
from django.conf.urls.static import static
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    path('', csrf_exempt(MedicineSearchView.as_view()), name='medicinesearch'),
    path('prescriptionchecker/', csrf_exempt(PrescriptionCheckerView.as_view()), name='prescriptionchecker'),
    path('dosage/', csrf_exempt(DosageView.as_view()), name='dosage'),
    # path('imagedetection/', csrf_exempt(ImageDetectionView.as_view()), name='imagedetection'),
    path('fakenewsdetection/', csrf_exempt(FakeNewsDetectionView.as_view()), name='fakenewsdetection')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)