from django.apps import AppConfig


class MedicinesearchConfig(AppConfig):
    name = 'medicinesearch'
