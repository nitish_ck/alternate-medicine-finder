from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import TemplateView, View
from django.conf import settings
import os
from collections import defaultdict
import pandas as pd

# import numpy as np
# import cv2
import pickle

# import s3fs
# from nltk.tokenize import RegexpTokenizer
# from sklearn.feature_extraction.text import TfidfVectorizer
# from sklearn.decomposition import TruncatedSVD
# from sklearn import preprocessing
import scipy.sparse as sp
# from scipy.sparse import csr_matrix
# from sklearn.linear_model import PassiveAggressiveClassifier
# from sklearn.metrics import accuracy_score, confusion_matrix
# from sklearn.linear_model import LogisticRegression
# from sklearn.ensemble import RandomForestRegressor
# from sklearn import metrics
# from sklearn.model_selection import train_test_split

# Create your views here.
# from pyspark.ml.feature import StopWordsRemover


class MedicineSearchView(TemplateView):
    # template_name = 'Face detection.html'
    template_name = 'index.html'

    # def get_context_data(self, **kwargs):
    #     context = super(MedicineSearchView, self).get_context_data(**kwargs)
    #     csv_path = os.path.join(settings.BASE_DIR, 'medicine.csv')
    #     fh = open(csv_path)
    #     fetchdata = fh.readlines()
    #     fh.close()
    #     b= fetchdata[0]
    #
    #
    #     def pre1(b):
    #         """Separates the row from CSV file as Brand Name, Chemicals, Dosages"""
    #         b = b.rstrip('\n')
    #         b = b.upper()
    #         b = b.split(',')
    #         return (b)
    #
    #     c = list(map(pre1, fetchdata))
    #     del (c[0])
    #
    #     MedDict = {}
    #     for i in c:
    #         if i[0] in MedDict.keys():
    #             pass
    #         else:
    #             MedDict[i[0]] = {'M': 1, 'dos': i[1], 'gName': i[1]}
    #
    #     ourTreeSearch = AVLTree()
    #     for i in MedDict.keys():
    #         ourTreeSearch.insert(
    #             i, MedDict[i]['M'], MedDict[i]['dos'], MedDict[i]['gName'],1)
    #
    #     context['avltree'] = str(ourTreeSearch.halfSearch('')).replace('(', '').replace(')', '').replace(' None,', '').rstrip("', None").lstrip("''").split("', '")
    #     return context

    def post(self, request):
        search = request.POST.get('search')
        if search:
            search = search.upper()
        # csv_path = os.path.join(settings.BASE_DIR, 'medicine.csv')
        # fh = open(csv_path)
        # fetchdata = fh.readlines()
        # fh.close()
        # del (fetchdata[0])
        # x = lambda y: y.replace(',,,,,,,,,,,,', '')
        # fetchdata = list(map(x, fetchdata))
        # MedDict = {}
        # # An array for brand names alone based on graph id
        # MedArray = {}
        #
        # def pre1(b):
        #     """Separates the row from CSV file as Brand Name, Chemicals, Dosages"""
        #     b = b.rstrip('\n')
        #     b = b.upper()
        #     b = b.split(',')
        #     return (b)
        #
        # c = list(map(pre1, fetchdata))
        #
        # MedGraph = defaultdict(list)
        # graph_index = 0
        #
        # medicine_graph_index = 0
        #
        # m = 0
        # for p in range(len(c)):
        #
        #     # If brand is already added but different dosage is found
        #     if c[p][0] in MedDict.keys():
        #         doses = len(MedDict[c[p][0]]['dos'])
        #         MedDict[c[p][0]]['dos'][doses + 1] = c[p][2]
        #     else:
        #
        #         # build our graph - adjacency list
        #
        #         # add medicine to graph
        #         MedGraph[graph_index] = []
        #         medicine_graph_index = graph_index
        #
        #         # create MedDict entry with graph index
        #         MedDict[c[p][0]] = {'M': 1, 'gens': c[p][1], 'dos': {0: c[p][2]}, 'gid': graph_index}
        #         MedArray[graph_index] = c[p][0]
        #         graph_index = graph_index + 1
        #
        #         for i in range(len(c[p][1])):
        #
        #             # If generic already in graph. Make connections
        #             if c[p][1][i] in MedDict:
        #                 chem_graph_index = MedDict[c[p][1][i]]['gid']
        #                 MedGraph[medicine_graph_index].append(chem_graph_index)
        #                 MedGraph[chem_graph_index].append(medicine_graph_index)
        #
        #             # If generic not in graph, create an entry with graph_index
        #             else:
        #                 MedGraph[graph_index] = []
        #                 chem_graph_index = graph_index
        #                 MedDict[c[p][1][i]] = {'M': 0, 'gens': '', 'dos': '', 'gid': chem_graph_index}
        #                 graph_index = graph_index + 1
        #
        #                 # make connections in graph
        #                 MedGraph[medicine_graph_index].append(chem_graph_index)
        #                 MedGraph[chem_graph_index].append(medicine_graph_index)
        #
        # ourTree = AVLTree()
        #
        # for i in MedDict.keys():
        #     ourTree.insert(
        #         i, MedDict[i]['M'], MedDict[i]['gens'], MedDict[i]['dos'], MedDict[i]['gid'])
        # avlsearchresult = ourTree.search(search)
        # if avlsearchresult != 'Not Found!':
        #     generic_name = avlsearchresult[2]
        #     generic_name_str = generic_name
        # else:
        #     generic_name_str = 'Generic name not found in dataset please check the brand name'
        #
        # result = []
        # all_gens = []
        # all_brands = []
        # index_node = ourTree.search(search)
        # if (index_node == -1):
        #     return 0
        # index = index_node[4]
        # no_of_gens = len(MedGraph[index])
        # for gens in MedGraph[index]:
        #     all_gens.append(gens)
        # for gens in all_gens:
        #     all_brands = all_brands + MedGraph[gens]
        #
        # list_set = set(all_brands)
        #
        # for brans in all_brands:
        #     if (set(MedGraph[index]) == set(MedGraph[brans])):
        #         if (MedArray[brans] != search):
        #             result.append(MedArray[brans])
        #
        # alternate_medicine_str = ''
        # if result:
        #     for eachmedicine in result:
        #         if not eachmedicine in alternate_medicine_str:
        #             alternate_medicine_str = '<strong>. </strong>' + eachmedicine + '<br>' + alternate_medicine_str
        # else:
        #     alternate_medicine_str = 'No Alternative Medicine Found'

        return JsonResponse(
            {'status': 1})


class Node():
    def __init__(self, key, M, gens, dos, gid):
        self.key = key
        self.M = M
        self.gens = gens
        self.dos = dos
        self.gid = gid
        self.left = None
        self.right = None


class AVLTree():
    def __init__(self, *args):
        self.node = None
        self.height = -1
        self.balance = 0;

        if len(args) == 1:
            for i in args[0]:
                self.insert(i)

    def height(self):
        if self.node:
            return self.node.height
        else:
            return 0

    def is_leaf(self):
        return (self.height == 0)

    def insert(self, key, M, gens, dos, gid):
        tree = self.node

        newnode = Node(key, M, gens, dos, gid)

        if tree == None:
            self.node = newnode
            self.node.left = AVLTree()
            self.node.right = AVLTree()
            # print("Inserted key [" + str(key) + "]")

        elif key < tree.key:
            self.node.left.insert(key, M, gens, dos, gid)

        elif key > tree.key:
            self.node.right.insert(key, M, gens, dos, gid)

        else:
            print("Key [" + str(key) + "] already in tree.")

        self.rebalance()

    def search(self, key, L=0):
        tree = self.node
        if tree == None:
            return (0)
        elif tree.key == key:
            return tree.key, tree.M, tree.gens, tree.dos, tree.gid, L
        elif key < tree.key:
            return tree.left.search(key, L + 1)
        elif key > tree.key:
            return tree.right.search(key, L + 1)

    def halfSearch(self, key, foundAlready=False):
        tree = self.node
        if tree == None:
            return
        if not foundAlready:
            if len(tree.key) >= len(key):
                if tree.key[:len(key)] == key:
                    return tree.key, tree.left.halfSearch(key, True), tree.right.halfSearch(key, True)
                elif tree.key[:len(key)] < key:
                    return tree.right.halfSearch(key, False)
                elif tree.key[:len(key)] > key:
                    return tree.left.halfSearch(key, False)
            else:
                if tree.key[:len(key)] < key:
                    return tree.right.halfSearch(key, False)
                elif tree.key[:len(key)] > key:
                    return tree.left.halfSearch(key, False)
        elif foundAlready:
            if len(tree.key) >= len(key):
                if tree.key[:len(key)] == key:
                    return tree.key, tree.left.halfSearch(key, True), tree.right.halfSearch(key, True)
                else:
                    return
            else:
                return tree.left.halfSearch(key, True), tree.left.halfSearch(key, True)

    def searchByLevel(self, L, withDetails=False, allTillHere=False, Count=0):
        '''Do not put value for count.'''
        if Count == L and withDetails == True:
            return self.node.key, self.node.M, self.node.gens, self.node.dos
        elif Count == L and withDetails == False:
            return self.node.key
        elif self.node.left.node == None and self.node.right.node != None:
            if allTillHere == True and withDetails == False:
                return self.node.key, self.node.right.searchByLevel(L, withDetails, allTillHere, Count + 1)
            elif allTillHere == True and withDetails == True:
                return self.node.key, self.node.M, self.node.gens, self.node.dos, self.node.right.searchByLevel(L,
                                                                                                                withDetails,
                                                                                                                allTillHere,
                                                                                                                Count + 1)
            else:
                return self.node.right.searchByLevel(L, withDetails, allTillHere, Count + 1)
        elif self.node.left.node != None and self.node.right.node == None:
            if allTillHere == True and withDetails == False:
                return self.node.key, self.node.left.searchByLevel(L, withDetails, allTillHere, Count + 1)
            elif allTillHere == True and withDetails == True:
                return self.node.key, self.node.M, self.node.gens, self.node.dos, self.node.left.searchByLevel(L,
                                                                                                               withDetails,
                                                                                                               allTillHere,
                                                                                                               Count + 1)
            else:
                return self.node.left.searchByLevel(L, withDetails, allTillHere, Count + 1)
        elif self.node.left.node != None and self.node.right.node != None:
            if allTillHere == True and withDetails == False:
                return self.node.key, self.node.left.searchByLevel(L, withDetails, allTillHere,
                                                                   Count + 1), self.node.right.searchByLevel(L,
                                                                                                             withDetails,
                                                                                                             allTillHere,
                                                                                                             Count + 1)
            elif allTillHere == True and withDetails == True:
                return self.node.key, self.node.M, self.node.gens, self.node.dos, self.node.left.searchByLevel(L,
                                                                                                               withDetails,
                                                                                                               allTillHere,
                                                                                                               Count + 1), self.node.right.searchByLevel(
                    L, withDetails, allTillHere, Count + 1)
            else:
                return self.node.left.searchByLevel(L, withDetails, allTillHere,
                                                    Count + 1), self.node.right.searchByLevel(L, withDetails,
                                                                                              allTillHere, Count + 1)

    def rebalance(self):
        '''
        Rebalance a particular (sub)tree
        '''
        # key inserted. Let's check if we're balanced
        self.update_heights(False)
        self.update_balances(False)
        while self.balance < -1 or self.balance > 1:
            if self.balance > 1:
                if self.node.left.balance < 0:
                    self.node.left.lrotate()  # we're in case II
                    self.update_heights()
                    self.update_balances()
                self.rrotate()
                self.update_heights()
                self.update_balances()

            if self.balance < -1:
                if self.node.right.balance > 0:
                    self.node.right.rrotate()  # we're in case III
                    self.update_heights()
                    self.update_balances()
                self.lrotate()
                self.update_heights()
                self.update_balances()

    def rrotate(self):
        # Rotate left pivoting on self
        print('Rotating ' + str(self.node.key) + ' right')
        A = self.node
        B = self.node.left.node
        T = B.right.node

        self.node = B
        B.right.node = A
        A.left.node = T

    def lrotate(self):
        # Rotate left pivoting on self
        print('Rotating ' + str(self.node.key) + ' left')
        A = self.node
        B = self.node.right.node
        T = B.left.node

        self.node = B
        B.left.node = A
        A.right.node = T

    def update_heights(self, recurse=True):
        if not self.node == None:
            if recurse:
                if self.node.left != None:
                    self.node.left.update_heights()
                if self.node.right != None:
                    self.node.right.update_heights()

            self.height = max(self.node.left.height,
                              self.node.right.height) + 1
        else:
            self.height = -1

    def update_balances(self, recurse=True):
        if not self.node == None:
            if recurse:
                if self.node.left != None:
                    self.node.left.update_balances()
                if self.node.right != None:
                    self.node.right.update_balances()

            self.balance = self.node.left.height - self.node.right.height
        else:
            self.balance = 0

    def delete(self, key):
        # print("Trying to delete at node: " + str(self.node.key))
        if self.node != None:
            if self.node.key == key:
                print("Deleting ... " + str(key))
                if self.node.left.node == None and self.node.right.node == None:
                    self.node = None  # leaves can be killed at will
                # if only one subtree, take that
                elif self.node.left.node == None:
                    self.node = self.node.right.node
                elif self.node.right.node == None:
                    self.node = self.node.left.node

                # worst-case: both children present. Find logical successor
                else:
                    replacement = self.logical_successor(self.node)
                    if replacement != None:  # sanity check
                        print("Found replacement for " + str(key) + " -> " + str(replacement.key))
                        self.node.key = replacement.key

                        # replaced. Now delete the key from right child
                        self.node.right.delete(replacement.key)

                self.rebalance()
                return
            elif key < self.node.key:
                self.node.left.delete(key)
            elif key > self.node.key:
                self.node.right.delete(key)

            self.rebalance()
        else:
            return

    def logical_predecessor(self, node):
        '''
        Find the biggest valued node in LEFT child
        '''
        node = node.left.node
        if node != None:
            while node.right != None:
                if node.right.node == None:
                    return node
                else:
                    node = node.right.node
        return node

    def logical_successor(self, node):
        '''
        Find the smallest valued node in RIGHT child
        '''
        node = node.right.node
        if node != None:  # just a sanity check

            while node.left != None:
                print("LS: traversing: " + str(node.key))
                if node.left.node == None:
                    return node
                else:
                    node = node.left.node
        return node

    def check_balanced(self):
        if self == None or self.node == None:
            return True

        # We always need to make sure we are balanced
        self.update_heights()
        self.update_balances()
        return ((abs(self.balance) < 2) and self.node.left.check_balanced() and self.node.right.check_balanced())

    def inorder_traverse(self):
        if self.node == None:
            return []

        inlist = []
        l = self.node.left.inorder_traverse()
        for i in l:
            inlist.append(i)

        inlist.append(self.node.key)

        l = self.node.right.inorder_traverse()
        for i in l:
            inlist.append(i)

        return inlist

    def totalHeight(self):
        if self.node == None:
            return 0
        else:
            return 1 + max(self.node.left.totalHeight(), self.node.right.totaHeight())

    def display(self, level=0, pref=''):
        '''
        Display the whole tree. Uses recursive def.
        TODO: create a better display using breadth-first search
        '''
        self.update_heights()  # Must update heights before balances
        self.update_balances()
        if (self.node != None):
            print('-' * level * 2, pref, self.node.key, "[" + str(self.height) + ":" + str(self.balance) + "]",
                  'L' if self.is_leaf() else ' ', self.node.M, '\n', self.node.dos)
            if self.node.left != None:
                self.node.left.display(level + 1, '<')
            if self.node.left != None:
                self.node.right.display(level + 1, '>')


class PrescriptionCheckerView(TemplateView):
    template_name = 'prescriptionchecker.html'

    def post(self, request):

        search = request.POST.get('search')
        dosage = request.POST.get('dosage')
        if search:
            search = search.upper()
        csv_path = os.path.join(settings.BASE_DIR, 'medicine.csv')
        fh = open(csv_path)
        fetchdata = fh.readlines()
        fh.close()
        del (fetchdata[0])
        x = lambda y: y.replace(',,,,,,,,,,,,', '')
        fetchdata = list(map(x, fetchdata))
        MedDict = {}
        # An array for brand names alone based on graph id
        MedArray = {}

        def pre1(b):
            """Separates the row from CSV file as Brand Name, Chemicals, Dosages"""
            b = b.rstrip('\n')
            b = b.upper()
            b = b.split(',')
            return (b)

        c = list(map(pre1, fetchdata))

        MedGraph = defaultdict(list)
        graph_index = 0

        medicine_graph_index = 0

        m = 0
        for p in range(len(c)):

            # If brand is already added but different dosage is found
            if c[p][0] in MedDict.keys():
                doses = len(MedDict[c[p][0]]['dos'])
                MedDict[c[p][0]]['dos'][doses + 1] = c[p][2]
            else:

                # build our graph - adjacency list

                # add medicine to graph
                MedGraph[graph_index] = []
                medicine_graph_index = graph_index

                # create MedDict entry with graph index
                MedDict[c[p][0]] = {'M': 1, 'gens': c[p][1], 'dos': {0: c[p][2]}, 'gid': graph_index}
                MedArray[graph_index] = c[p][0]
                graph_index = graph_index + 1

                for i in range(len(c[p][1])):

                    # If generic already in graph. Make connections
                    if c[p][1][i] in MedDict:
                        chem_graph_index = MedDict[c[p][1][i]]['gid']
                        MedGraph[medicine_graph_index].append(chem_graph_index)
                        MedGraph[chem_graph_index].append(medicine_graph_index)

                    # If generic not in graph, create an entry with graph_index
                    else:
                        MedGraph[graph_index] = []
                        chem_graph_index = graph_index
                        MedDict[c[p][1][i]] = {'M': 0, 'gens': '', 'dos': '', 'gid': chem_graph_index}
                        graph_index = graph_index + 1

                        # make connections in graph
                        MedGraph[medicine_graph_index].append(chem_graph_index)
                        MedGraph[chem_graph_index].append(medicine_graph_index)
        table_html = ""
        generic_table_html = ""
        ourTree = AVLTree()

        for i in MedDict.keys():
            ourTree.insert(
                i, MedDict[i]['M'], MedDict[i]['gens'], MedDict[i]['dos'], MedDict[i]['gid'])
        avlsearchresult = ourTree.search(search)

        if avlsearchresult != 'Not Found!':
            generic_name = avlsearchresult[2]
            dosagedetails = avlsearchresult[3]
            generic_name_str = generic_name
        else:
            generic_name_str = 'Generic name not found in dataset please check the brand name'
        generic_name_dict = []
        dosage_dict = []
        table_html = "<tr id=" + str(1) + "><td>" + str(search) + "(" + str(generic_name) + ")" + "</td></tr>"
        generic_name_str = generic_name_str.split("+")
        dosagedetails = dosage.split("+")
        i = 0
        for generic_name_strs in generic_name_str:
            generic_table_html += "<tr id=" + str(1) + "><td>" + str(generic_name_strs) + "</td><td>" + str(
                dosagedetails[i]) + "</td></tr>"
            generic_name_dict.append(str(generic_name_strs))
            dosage_dict.append(str(dosagedetails[i]))

            i += 1

        return JsonResponse(
            {'status': 1, 'table_html': table_html, 'generic_table_html': generic_table_html,
             'generic_name_dict': generic_name_dict, 'dosage_dict': dosage_dict})


class DosageView(View):

    def post(self, request):

        search = request.POST.get('search')
        if search:
            search = search.upper()
        csv_path = os.path.join(settings.BASE_DIR, 'medicine.csv')
        fh = open(csv_path)
        fetchdata = fh.readlines()
        fh.close()
        del (fetchdata[0])
        x = lambda y: y.replace(',,,,,,,,,,,,', '')
        fetchdata = list(map(x, fetchdata))
        MedDict = {}
        # An array for brand names alone based on graph id
        MedArray = {}

        def pre1(b):
            """Separates the row from CSV file as Brand Name, Chemicals, Dosages"""
            b = b.rstrip('\n')
            b = b.upper()
            b = b.split(',')
            return (b)

        c = list(map(pre1, fetchdata))

        MedGraph = defaultdict(list)
        graph_index = 0

        medicine_graph_index = 0

        m = 0
        for p in range(len(c)):

            # If brand is already added but different dosage is found
            if c[p][0] in MedDict.keys():
                doses = len(MedDict[c[p][0]]['dos'])
                MedDict[c[p][0]]['dos'][doses + 1] = c[p][2]
            else:

                # build our graph - adjacency list

                # add medicine to graph
                MedGraph[graph_index] = []
                medicine_graph_index = graph_index

                # create MedDict entry with graph index
                MedDict[c[p][0]] = {'M': 1, 'gens': c[p][1], 'dos': {0: c[p][2]}, 'gid': graph_index}
                MedArray[graph_index] = c[p][0]
                graph_index = graph_index + 1

                for i in range(len(c[p][1])):

                    # If generic already in graph. Make connections
                    if c[p][1][i] in MedDict:
                        chem_graph_index = MedDict[c[p][1][i]]['gid']
                        MedGraph[medicine_graph_index].append(chem_graph_index)
                        MedGraph[chem_graph_index].append(medicine_graph_index)

                    # If generic not in graph, create an entry with graph_index
                    else:
                        MedGraph[graph_index] = []
                        chem_graph_index = graph_index
                        MedDict[c[p][1][i]] = {'M': 0, 'gens': '', 'dos': '', 'gid': chem_graph_index}
                        graph_index = graph_index + 1

                        # make connections in graph
                        MedGraph[medicine_graph_index].append(chem_graph_index)
                        MedGraph[chem_graph_index].append(medicine_graph_index)

        ourTree = AVLTree()

        for i in MedDict.keys():
            ourTree.insert(
                i, MedDict[i]['M'], MedDict[i]['gens'], MedDict[i]['dos'], MedDict[i]['gid'])
        avlsearchresult = ourTree.search(search)
        if avlsearchresult != 'Not Found!':
            dosagedetails = avlsearchresult[3]
        else:
            dosagedetails = 'dosage not found in dataset please check the brand name'

        dosage_select = ""
        for dosagedetail in dosagedetails:
            dosage_select += "<option value=" + str(dosagedetails[dosagedetail]) + ">" + str(
                dosagedetails[dosagedetail]) + "</option>"

        return JsonResponse(
            {'status': 1, 'dosage_select': dosage_select})


# class ImageDetectionView(View):
#
#     def post(self, request):
#
#         classifier = request.POST.get('classifier')
#         if classifier == 'Logistic Regression':
#             face_cascade = cv2.CascadeClassifier('cascade\data\haarcascade_frontalface_alt2.xml')
#             recognizer = cv2.face.LBPHFaceRecognizer_create()
#             recognizer.read("trainner.yml")
#             labels = {}
#             with open("labels.pickle", 'rb') as f:
#                 og_labels = pickle.load(f)
#                 labels = {v: k for k, v in og_labels.items()}
#
#             cap = cv2.VideoCapture(0)
#
#             while (True):
#                 # Capture Frame by Frame
#                 ret, frame = cap.read()
#                 # Convert into grey Scale
#                 grey = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
#
#                 faces = face_cascade.detectMultiScale(grey, scaleFactor=1.5, minNeighbors=5)
#                 for (x, y, h, w) in faces:
#                     roi_grey = grey[y:y + h, x:x + w]
#                     roi_color = frame[y:y + h, x:x + w]
#
#                     # Recognizer
#                     id_, conf = recognizer.predict(roi_grey)
#                     if conf >= 45:
#                         print(id_, conf)
#                         print(labels[id_])
#                         font = cv2.FONT_HERSHEY_SIMPLEX
#                         name = labels[id_]
#                         color = (255, 255, 255)
#                         stroke = 2
#                         cv2.putText(frame, name, (x, y), font, 1, color, stroke, cv2.LINE_AA)
#                     img_item = "nitishck.png"
#                     cv2.imwrite(img_item, roi_color)
#
#                     # Draw rectangle
#                     color = (255, 0, 0)
#                     stroke = 2
#                     end_cord_x = x + w
#                     end_cord_y = y + h
#                     cv2.rectangle(frame, (x, y), (end_cord_x, end_cord_y), color, stroke)
#
#                 # Display the resulting Frame
#                 cv2.imshow('frame', frame)
#                 cv2.setWindowProperty('frame', cv2.WND_PROP_TOPMOST, 1)
#                 if cv2.waitKey(20) & 0xFF == ord('q'):
#                     break
#                 if cv2.getWindowProperty('frame', cv2.WND_PROP_VISIBLE) < 1:
#                     break
#
#             # When every thing done release the capture
#             cap.release()
#             cv2.destroyAllWindows()
#
#         return JsonResponse(
#             {'status': 1})


class FakeNewsDetectionView(View):

    def post(self, request):

        title = request.POST.get('title')
        topic = request.POST.get('topic')
        newscategory = request.POST.get('category')

        # tf1_new = TfidfVectorizer(analyzer='word', stop_words="english", vocabulary=tf1.vocabulary_)

        # subject2 = le.transform(subject)

        # combined_2 = sp.hstack([title2, text2, sub2])
        # combined_3 = sp.hstack([title3, text3, sub3])

        model = pickle.load(open(settings.MODEL, 'rb'))
        voc1 = pickle.load(open(settings.VOC1, 'rb'))
        voc2 = pickle.load(open(settings.VOC2, 'rb'))
        voc3 = pickle.load(open(settings.VOC3, 'rb'))
        combined = ''
        title = voc1.transform([title])
        topic = voc2.transform([topic])
        newscategory = voc3.transform([newscategory])
        combined = sp.hstack([title, topic, newscategory])
        print(model.predict(combined))
        output = model.predict(combined)
        if (output[0] == 0) or (output[0] == 1):
            result = str(output[0])
            return JsonResponse({'status': 1, 'result': result})
        else:
            return JsonResponse({'status': -1})
